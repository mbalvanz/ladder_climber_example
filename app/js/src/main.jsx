/** @jsx React.DOM */
var app = (function() {
  var self = this;
  self.Climber = React.createClass({
    render: function() {
      var classes = this.props.climbing ? 'climbing' : '';
      var src = 'assets/climber.ico';
      return (
        <img id="climber" src={src} className={classes} width="64" height="64"/>
      )
    }
  });

  self.Banner = React.createClass({
    render: function() {
      return (
        <div className="banner">{this.props.message}</div>
      )
    }
  })

  self.MainView = React.createClass({
    getInitialState: function() {
      return {climbing: false}
    },
    handleContextMenu: function(evt) {
      evt.preventDefault()
    },
    handleMouseDown: function(evt) {
      this.setState({climbing: true, message: ''});
      evt.preventDefault();
    },
    handleMouseUp: function() {
      this.setState({climbing: false, message: 'You are winner!'})
    },
    render: function() {
      return (
        <div id="game-view"
             onContextMenu={this.handleContextMenu}
             onMouseDown={this.handleMouseDown}
             onMouseUp={this.handleMouseUp}
             onTouchStart={this.handleMouseDown}
             onTouchEnd={this.handleMouseUp}>
             <Banner message={this.state.message} />
             <Climber climbing={this.state.climbing} />
        </div>
      )
    }
  });

  React.renderComponent(
    <MainView />,
    document.getElementById('main')
  )
})();
