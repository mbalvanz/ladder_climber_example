Ladder Climber
==============
Ladder Climber is an extremely simple "game" I assembled to demonstrate how to
create applications using Elm.

Setup
=====
1. Clone the repository
2. Install [Elm](https://guide.elm-lang.org/get_started.html)
3. Compile:

        $ elm-make ./elm/Main.elm --output=./app/js/dist/main.js
        Some new packages are needed. Here is the upgrade plan.                                       
                                                                                                    
        Install:                                                                                    
            elm-community/elm-test 1.1.0                                                              
            elm-lang/core 4.0.5                                                                       
            elm-lang/html 1.1.0                                                                       
            elm-lang/virtual-dom 1.1.1                                                                
            knledg/touch-events 2.0.0                                                                 
                                                                                                    
        Do you approve of this plan? [Y/n] y                                                          
        Starting downloads...                                                                         
                                                                                                    
        * knledg/touch-events 2.0.0                                                                 
        * elm-lang/virtual-dom 1.1.1                                                                
        * elm-lang/html 1.1.0                                                                       
        * elm-lang/core 4.0.5                                                                       
                                                                                                    
        * elm-community/elm-test 1.1.0                                                                
        Packages configured successfully!

4. Start serving the files with a webserver like Python:
    
        $ python -m SimpleHTTPServer

5. Navigate to http://localhost:8000/app
