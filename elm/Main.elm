import Json.Decode as Json
import Html exposing (..)
import Html.App as App
import Html.Attributes exposing (..)
import Html.Events exposing (onMouseDown, onMouseUp, onMouseOut, onWithOptions, Options)
import Time exposing (Time, second)
import TouchEvents exposing (onTouchStart, onTouchEnd)


onContextMenu : msg -> Attribute msg
onContextMenu message =
  onWithOptions "contextmenu" (Options True True) (Json.succeed message)


climb: Model -> (Model, Cmd Msg)
climb model = 
  let
    newPosition =
      if model.position == ClimbingLeft then
        ClimbingRight
      else
        ClimbingLeft
  in
    ({ model | position = newPosition, score = model.score + 1 }, Cmd.none)


init : ( Model, Cmd Msg )
init =
  ( model, Cmd.none )


scoreboard: Model -> Html Msg
scoreboard model = 
  let
    message =
      if model.neverClimbed then "Ladder Climber!" else "Score: " ++ toString model.score
  in
    div [ class "scoreboard" ] [ text message ]

main: Program Never
main =
  App.program 
    { init = init
    , view = view
    , subscriptions = subscriptions
    , update = update
    }

-- MODEL

type ClimberPosition
  = ClimbingLeft
  | ClimbingRight

type alias Model = { climbing: Bool
                   , score: Int
                   , winner: Bool
                   , position: ClimberPosition
                   , neverClimbed: Bool
                   }

model: Model
model = 
  Model False 0 False ClimbingRight True


-- UPDATE

type Msg
  = Reset 
  | StartClimbing
  | StartClimbingTouch TouchEvents.Touch
  | StopClimbing
  | StopClimbingTouch TouchEvents.Touch
  | Tick Time

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Reset -> 
      ({ model | score = 0 }, Cmd.none)
    StartClimbing ->
      ({ model | climbing = True, neverClimbed = False, score = 0, winner = False }, Cmd.none)
    StartClimbingTouch _ ->
      ({ model | climbing = True, neverClimbed = False, score = 0, winner = False }, Cmd.none)
    StopClimbing ->
      ({ model | climbing = False, winner = True }, Cmd.none)
    StopClimbingTouch _ ->
      ({ model | climbing = False, winner = True }, Cmd.none)
    Tick _ ->
      climb model


-- VIEW

view : Model -> Html Msg
view model =
  let
    banner =
      if model.winner && model.score > 0 then
        div [ class "banner" ] [ text "You are winner!" ]
      else
        div [] []
    imgClass =
      "climbing " ++ (if model.position == ClimbingLeft then "left" else "right")
  in
    div []
      [ div
          [ id "game-view"
          , onMouseDown StartClimbing
          , onMouseOut StopClimbing
          , onMouseUp StopClimbing
          , onContextMenu StopClimbing
          , onTouchStart StartClimbingTouch
          , onTouchEnd StopClimbingTouch
          ]
          [ banner
          , scoreboard model
          , img 
              [ id "climber"
              , src "assets/climber.ico"
              , height 64
              , width 64
              , class imgClass
              ] []
          ]
      ]

-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
  let
    subs = 
      if model.climbing then
        [(Time.every second Tick)]
      else
        []
  in
    Sub.batch subs